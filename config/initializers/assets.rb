# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.precompile += %w( vendor/blue.css.scss )
Rails.application.config.assets.precompile += %w( vendor/select2.min.css )
Rails.application.config.assets.precompile += %w( vendor/bootstrap.min.css )
Rails.application.config.assets.precompile += %w( vendor/bootstrap.css )
Rails.application.config.assets.precompile += %w( vendor/bootstrap-theme.min.css )
Rails.application.config.assets.precompile += %w( vendor/bootstrap-theme.css )
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.precompile += %w( vendor/blue )
Rails.application.config.assets.precompile += %w( vendor/icheck.js )
Rails.application.config.assets.precompile += %w( jquery3.js )
Rails.application.config.assets.precompile += %w( jquery_ujs )
Rails.application.config.assets.precompile += %w( vendor/select2.min.js )
