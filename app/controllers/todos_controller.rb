class TodosController < ApplicationController
  protect_from_forgery except: [:index, :create]
  def index
    @projects = Project.all
    @todo = Todo.new
	respond_to do |format| 
	  format.html
	  format.json { render json: @projects, include: :todos  }
	end
  end

  def edit
    @todo = Todo.find(params[:id])
    @todo[:isCompleted] ^= true
    @todo.save
    redirect_to todos_path
  end

  def new
    @projects = Project.all
  end

  def show
    redirect_to todos_path
  end

  def create
    @todo = Todo.new(params.require(:todo).permit(:text, :project_id))
	@todo[:isCompleted] = false
    @todo.save
    redirect_to todos_path
  end
end
