// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require jquery_ujs
//= require icheck
//= require select2.min
//= require_tree .

$(document).ready(function () {

    //initializers
    var todo_checkbox = $('input');
    var todo_checkbox_inside = $('input[type=checkbox]');
    var select = $('select');
    select.select2();
    select.select2({
        minimumResultsForSearch: -1
    });
    todo_checkbox.on('ifCreated', function(event){
        todo_checkbox_inside.val(function (index, value) {
            if(this.checked){
               $('label[for=\'todo_label_'+value+'\']').css("text-decoration", "line-through");
            }
        });
    });
    todo_checkbox.iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
        //increaseArea: '20%' // optional
    });



    //onclick listeners

   $('#show_new').click(function (event) {
       $('.enveloper').show();
       event.preventDefault();
   });
   $('#hide_new').click(function (event) {
       $('.enveloper').hide();
       event.preventDefault();
   });
    $('#ok_button').click(function (event) {
        $('form').submit();
       event.preventDefault();
   });
    todo_checkbox.on('ifChanged', function (event) { $(event.target).trigger('change'); });

    todo_checkbox_inside.change(function (e) {
        var num = this.id.match(/\d+/)[0];
        if(this.checked){
            $('label[for=\'todo_label_'+num+'\']').css("text-decoration", "line-through");
        }
        else{
            $('label[for=\'todo_label_'+num+'\']').css("text-decoration", "");
        }
        $.get('/todos/'+num+'/edit');
    })

});
