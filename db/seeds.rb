# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'dropbox_sdk'
require 'yaml'

ACCESS_TOKEN = 'Oq4WUa9ojzAAAAAAAAAAGESEzRKZllCyC1dr1nE_P4AP3HzBsWhEF90klMY3TWsM'

client = DropboxClient.new(ACCESS_TOKEN)

content = client.get_file('/seeds.yml')

data = HashWithIndifferentAccess.new(YAML.safe_load(content))

data[:projects].each do |project|
  @project = Project.new(title: project[:title])
  @project.save
  project[:todos].each do |todo|
    @todo = Todo.new(text: todo[:text],
                     isCompleted: todo[:isCompleted],
                     project_id: @project[:id])
    @todo.save
  end
end